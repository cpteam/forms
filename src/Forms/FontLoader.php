<?php

namespace CPTeam\Forms\Controls;

use Nette\Application\UI\Control;

class FontLoader extends Control
{
	private $el;
	
	/**
	 * FontLoader constructor.
	 *
	 * @param $fonts
	 */
	public function __construct($fonts)
	{
		if (is_array($fonts)) {
			$fonts = implode("|", $fonts);
		}
		
		$this->el = \Nette\Utils\Html::el('link', [
			"href" => "https://fonts.googleapis.com/css?family=$fonts",
			"rel" => "stylesheet",
		]);
	}
	
	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->el->__toString();
	}
}


