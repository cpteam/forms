<?php

namespace CPTeam\Forms\Controls;

use CPTeam\Forms\CoreFormException;
use Nette\Forms\Controls as NFC;

const GRID_SIZE_MIN = 1;
const GRID_SIZE_MAX = 12;

interface Rendable
{
	public function render();
}

interface Gridable
{
	public function setGridSize($value);
	
	public function getGridSize();
}

trait TGridable
{
	protected $gridSize = GRID_SIZE_MAX;
	
	public function setGridSize($value)
	{
		if ($value > GRID_SIZE_MAX || $value < GRID_SIZE_MIN) {
			throw new CoreFormException("Grid size must be bewteen " . GRID_SIZE_MAX . " and " . GRID_SIZE_MAX);
		}
		
		$this->gridSize = (int)$value;
		
		return $this;
	}
	
	public function getGridSize()
	{
		return $this->gridSize;
	}
}

class Button extends NFC\Button implements Gridable
{
	use TGridable;
}

class Checkbox extends NFC\Checkbox implements Gridable
{
	use TGridable;
}

class CheckboxList extends NFC\CheckboxList implements Gridable
{
	use TGridable;
}

class HiddenField extends NFC\HiddenField implements Gridable
{
	use TGridable;
}

class MultiSelectBox extends NFC\MultiSelectBox implements Gridable
{
	use TGridable;
}

class ImageButton extends NFC\ImageButton implements Gridable
{
	use TGridable;
}

class RadioList extends NFC\RadioList implements Gridable
{
	use TGridable;
}

class SelectBox extends NFC\SelectBox implements Gridable
{
	use TGridable;
}

class SubmitButton extends NFC\SubmitButton implements Gridable
{
	use TGridable;
}

class TextArea extends NFC\TextArea implements Gridable
{
	use TGridable;
	
	public function setAutosize()
	{
		$this->setAttribute('class', $this->control->getAttribute('class') . ' autosize');
		
		return $this;
	}
	
}

class TextInput extends NFC\TextInput implements Gridable
{
	use TGridable;
}

class UploadControl extends NFC\UploadControl implements Gridable
{
	use TGridable;
}

