<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 23.10.16
 * Time: 19:04
 */

namespace CPTeam\Forms\Controls;

use CPTeam\Forms\Container;

/**
 * Class ImageCropperContainer
 *
 * @package vendor\cosplan\core\src\Nette\Forms\Controls
 */
class ImageCropperContainer extends Container
{
	
	public $height;
	public $width;
	public $ratio;
	
	private $labelName;
	
	/**
	 * ImageCropperContainer constructor.
	 *
	 * @param $labelName
	 */
	public function __construct($parent, $name, $labelName)
	{
		parent::__construct($parent, $name);
		
		$this->labelName = $labelName;
	}
	
	public function build()
	{
		$this->addUpload('image', $this->labelName);
		
		//$this->addHidden('data');
		//$this->addHidden('base64', null, $this->getId() . 'InputBase64');
		
		$this->addHidden('sourceX', null, $this->getId() . 'sourceX');
		$this->addHidden('sourceY', null, $this->getId() . 'sourceY');
		$this->addHidden('sourceWidth', null, $this->getId() . 'sourceWidth');
		$this->addHidden('sourceHeight', null, $this->getId() . 'sourceHeight');
	}
	
	/**
	 * @param mixed $ratio
	 *
	 * @return ImageCropperContainer
	 */
	public function setRatio($ratio)
	{
		$this->ratio = $ratio;
		
		return $this;
	}
	
	/**
	 * @param mixed $width
	 *
	 * @return ImageCropperContainer
	 */
	public function setWidth($width)
	{
		$this->width = $width;
		
		return $this;
	}
	
	/**
	 * @param mixed $height
	 *
	 * @return ImageCropperContainer
	 */
	public function setHeight($height)
	{
		$this->height = $height;
		
		return $this;
	}
	
}
