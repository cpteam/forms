<?php

namespace CPTeam\Forms\Controls;

use CPTeam\Forms\Container;
use Latte\Engine;

/**
 * Class EmailTextInput
 */
class MarkdownContainer extends Container implements Rendable
{
	
	public function prepare()
	{
		$this->addText("title", "Nadpis")
			->setAttribute('class', "markdownTitle");
		
		$this->addTextArea("perex", "Perex")
			->setAttribute('class', "markdownPerex")
			->setAttribute('style', 'font-size: 18px;line-height: 1.6;')
			->setAutosize();
		
		$this->addTextArea("text", "Obsah")
			->setAttribute('class', "markdownText")
			->setAttribute('style', 'font-size: 18px;line-height: 1.6;')
			->setAutosize();
	}

	public function render()
	{
		/** @var Engine $latte */
		$latte = $this->getForm()->getRenderer()->getTemplate()->getLatte();
		
		$arr = [
			'control' => $this,
			'form' => $this->getForm(),
			"title" => $this['title'],
			"perex" => $this['perex'],
			"text" => $this['text'],
		];
		
		$latte->render(__DIR__ . "/markdown.latte", $arr);
	}
}
