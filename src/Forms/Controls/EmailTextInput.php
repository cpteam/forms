<?php

namespace CPTeam\Forms\Controls;

use CPTeam\LogicException;
use CPTeam\Tracy\Helpers\BlueScreen;
use Nette\Forms\Form;
use Nette\Forms\IControl;
use Nette\Forms\Validator;

/**
 * Class EmailTextInput
 */
class EmailTextInput extends TextInput
{
	
	//u koncovky kontrolujeme pouze minimalni znaky, jelikoz v dnesni dobe jiz existuji domeny jako .company
	const PATTERN_EMAIL = '([\w\.\-]+@[\w\-]+\.[a-zA-Z]{2,})';
	
	const MSG_REQUIRED = 'common.form.email.required';
	const MSG_INVALID_EMAIL_PATTERN = 'common.form.email.invalid';
	const MSG_EMAIL_ALREADY_USED = 'common.form.email.alreadyUsed';
	
	const VALIDATE_EMAIL = self::CLASS . "::validateEmail";
	
	private $messageEmailAlreadyUsed = null;
	
	/**
	 * EmailTextInput constructor.
	 *
	 * @param null $label
	 * @param null $messageEmailInvalid
	 * @param null $messageEmailExists
	 */
	public function __construct($label = null, $messageEmailInvalid = null, $messageEmailExists = null)
	{
		parent::__construct($label);
		
		$this->messageEmailAlreadyUsed = $messageEmailExists !== null ? $messageEmailExists : self::MSG_EMAIL_ALREADY_USED;
		
		$this->addCondition(Form::FILLED)
			->addRule(self::VALIDATE_EMAIL, $messageEmailInvalid !== null ? $messageEmailInvalid : self::MSG_INVALID_EMAIL_PATTERN);
	}
	
	/**
	 * @param null $value
	 *
	 * @return $this
	 */
	public function setRequired($value = null)
	{
		parent::setRequired($value !== null ? $value : self::MSG_REQUIRED);
		
		return $this;
	}
	
	/**
	 * @param callable $closure
	 * @param null $message
	 *
	 * @return $this
	 */
	public function validateNoDuplicity(callable $closure, $message = null)
	{
		//if message is not defined, use class instance message
		$message = $message !== null ? $message : $this->messageEmailAlreadyUsed;
		
		// add onValidate handle to form
		$this->getForm()->onValidate[] = function (Form $form, $values) use ($closure, $message) {
			
			// get reflection from closure
			$reflection = new \ReflectionFunction($closure);
			$arguments = $reflection->getParameters();
			
			//Throws error if no arguments
			if (empty($arguments[0])) {
				
				//prepare bluescreen handle for our specific exception
				BlueScreen::addExceptionPreview(
					EmailTextInputException::class,
					'EmailTextInput::validateNoDuplicity usage',
					$reflection->getFileName(),
					$reflection->getStartLine()
				);
				
				//throw our exception
				throw new EmailTextInputException("Missing first argument defined in closure.");
			}
			
			//check if callback is false
			if ($closure($this->getValue()) === false) {
				// add error to input
				$this->addError(
					$form->getTranslator()->translate(
						$message
					)
				);
			}
		};
		
		return $this;
	}
	
	/**
	 * @param IControl $control
	 *
	 * @return bool
	 *
	 * @throws EmailTextInputException if used IControl isn't instance of this class
	 */
	public static function validateEmail(IControl $control)
	{
		if ($control instanceof self == false) {
			throw new EmailTextInputException('Email validation can be used only on `' . self::CLASS . "` control.");
		}
		
		return Validator::validatePattern($control, self::PATTERN_EMAIL);
	}
}

class EmailTextInputException extends LogicException
{
	
}
