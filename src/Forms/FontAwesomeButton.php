<?php
/**
 * Created by PhpStorm.
 * User: luky
 * Date: 8.6.16
 * Time: 13:47
 */

namespace CPTeam\Forms;

use Nette\Forms\Controls\SubmitButton;
use Nette\Utils\Html;

/**
 * Class FontAwesomeButton
 *
 * Inspired by https://gist.github.com/jkuchar/5025394
 *
 */
class FontAwesomeButton extends SubmitButton
{
	
	/** @var Html */
	private $icon = null;
	
	private $iconClass = 'fa-send';
	
	/**
	 * FontAwesomeButton constructor.
	 *
	 * @param null $icon
	 */
	public function __construct($icon = null)
	{
		parent::__construct();
		
		if ($icon) {
			$this->iconClass = $icon;
		}
		
		$this->control->setName("button")->type = 'submit';
		
		$this->control->class[] = "submit";
	}
	
	public function getIconPrototype()
	{
		
		if (!$this->icon) {
			$this->icon = Html::el("i")->addAttributes(['class' => 'fa ' . $this->iconClass]);
		}
		
		return $this->icon;
	}
	
	public function getControl($caption = null)
	{
		$control = parent::getControl($caption);
		$control->setHtml($this->getIconPrototype());
		
		return $control;
	}
	
}
