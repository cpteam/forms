<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 23.10.16
 * Time: 12:50
 */

namespace App\Components\Form\Render;

use CPTeam\Forms\Controls\FontLoader;
use CPTeam\Forms\Controls\Gridable;
use CPTeam\Forms\Controls\ImageCropperContainer;
use CPTeam\Forms\Controls\Rendable;
use CPTeam\LogicException;
use Latte\Engine;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Bridges\FormsLatte\FormMacros;
use Nette\Forms\Container;
use Nette\Forms\Controls\BaseControl;
use Nette\Forms\Controls\HiddenField;
use Nette\Forms\Form;
use Nette\Forms\IFormRenderer;

/**
 * Class FormRenderControl
 *
 * @package App\Components\Form\Render
 */
class GridFormRender implements IFormRenderer
{
	/** @var  Template */
	private $template;
	
	/**
	 * @param \Nette\Forms\Form $form
	 *
	 * @return string
	 */
	function render(Form $form)
	{
		$latte = new Engine();
		$latte->onCompile[] = function (Engine $latte) {
			FormMacros::install($latte->getCompiler());
		};
		
		$this->template = new Template($latte);
		
		$this->template->form = $form;
		
		$this->template->setFile(__DIR__ . "/form.latte");
		
		ob_start();
		$this->template->render();
		
		return ob_get_clean();
	}
	
	public function renderComponent($component)
	{
		if ($component instanceof HiddenField) {
			return;
		}
		
		$size = $component instanceof Gridable ? $component->getGridSize() : 12;
		$padding = $component instanceof Container ? 0 : 15;
		
		echo "<div class=\"col{$size} padding{$padding}\">";
		if ($component instanceof ImageCropperContainer) {
			$this->template->render(__DIR__ . DIRECTORY_SEPARATOR . "imageCropper.latte", [
				"component" => $component,
			]);
		} elseif ($component instanceof Rendable) {
			$component->render();
		} elseif ($component instanceof Container) {
			echo "<div class='grid'>";
			
			foreach ($component->getComponents() as $child) {
				$this->renderComponent($child);
			}
			
			echo "</div>";
		} elseif ($component instanceof BaseControl) {
			echo $component->getLabel();
			echo $component->getControl();
		} elseif ($component instanceof FontLoader) {
			echo $component;
		} else {
			throw new LogicException("Undefined component " . $component->name);
		}
		
		echo "</div>";
	}
	
	/**
	 * @return Template
	 */
	public function getTemplate()
	{
		return $this->template;
	}
	
	
}
