<?php
/**
 * Created by PhpStorm.
 * User: luky
 * Date: 7.7.16
 * Time: 14:02
 */

namespace CPTeam\Forms\Rendering;

use Nette\Forms\Rendering\DefaultFormRenderer;

/**
 * Class CoreFormRender
 *
 * @package CPTeam\Forms\Rendering
 */
class CoreFormRender extends DefaultFormRenderer
{
	
	/**
	 *  /--- form.container
	 *
	 *    /--- error.container
	 *      .... error.item [.class]
	 *    \---
	 *
	 *    /--- hidden.container
	 *      .... HIDDEN CONTROLS
	 *    \---
	 *
	 *    /--- group.container
	 *      .... group.label
	 *      .... group.description
	 *
	 *      /--- controls.container
	 *
	 *        /--- pair.container [.required .optional .odd]
	 *
	 *          /--- label.container
	 *            .... LABEL
	 *            .... label.suffix
	 *            .... label.requiredsuffix
	 *          \---
	 *
	 *          /--- control.container [.odd]
	 *            .... CONTROL [.required .text .password .file .submit .button]
	 *            .... control.requiredsuffix
	 *            .... control.description
	 *            .... control.errorcontainer + control.erroritem
	 *          \---
	 *        \---
	 *      \---
	 *    \---
	 *  \--
	 *
	 * @var array of HTML tags
	 */
	public $wrappers = [
		'form' => [
			'container' => 'div class=grid',
		],
		
		'error' => [
			'container' => 'ul class=error',
			'item' => 'li',
		],
		
		'group' => [
			'container' => 'div',
			'label' => 'div class=groupTitle',
			'description' => 'p',
		],
		
		'controls' => [
			'container' => null,
		],
		
		'pair' => [
			'container' => 'div class=col12',
			'.required' => 'required',
			'.optional' => null,
			'.odd' => null,
			'.error' => null,
		],
		
		'control' => [
			'container' => null,
			'.odd' => null,
			
			'description' => 'small',
			'requiredsuffix' => '',
			'errorcontainer' => 'span class=error',
			'erroritem' => '',
			
			'.required' => 'required',
			'.text' => 'text',
			'.password' => 'text',
			'.file' => 'text',
			'.submit' => 'button',
			'.image' => 'imagebutton',
			'.button' => 'button',
		],
		
		'label' => [
			'container' => null,
			'suffix' => null,
			'requiredsuffix' => null,
		],
		
		'hidden' => [
			'container' => 'div',
		],
	];
	
}
