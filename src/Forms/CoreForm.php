<?php
/**
 * Created by PhpStorm.
 * User: Lukys
 * Date: 17. 4. 2016
 * Time: 12:53
 */

namespace CPTeam\Forms;

use App\Components\Form\Render\GridFormRender;
use CPTeam\Forms\Controls;
use CPTeam\Forms\Controls\Gridable;
use CPTeam\Forms\Controls\TGridable;
use Nette\ComponentModel\IContainer;
use Nette\Utils\Html;
use Nette\Utils\Json;

/**
 * Class CoreForm
 *
 */
class CoreForm extends \Nette\Application\UI\Form
{
	
	use TForms;
	
	private $control = null;
	
	const ALIAS_PATTERN = '[a-z][a-z0-9.]+[a-z0-9]';
	
	/**
	 * CoreForm constructor.
	 *
	 * @param \Nette\ComponentModel\IContainer|null $parent
	 * @param null $name
	 */
	final public function __construct(IContainer $parent = null, $name = null)
	{
		$this->control = $parent;
		
		parent::__construct($parent = null, $name = null);
		
		$this->setRenderer(new GridFormRender());
		
		$this->prepare();
	}
	
	public function editable()
	{
		$this->addHidden('isEdit')->setDefaultValue(false);
		$this->addHidden('eid');
	}
	
	public function setEdit($arr)
	{
		$arr['isEdit'] = true;
		$this->setDefaults($arr);
		
		return $this;
	}
	
	public function isEdit()
	{
		return (bool)$this->getValues()['isEdit'];
	}
	
	public function prepare()
	{
		//implement in childs
	}
	
	/**
	 * @param bool $value
	 *
	 * @return $this
	 */
	public function setAjax($value = true)
	{
		if ($value) {
			$this->getElementPrototype()->setAttribute('class', 'ajax');
		}
		
		return $this;
	}
	
	public function addAlias()
	{
		$el = $this->addText(
			'alias',
			Html::el('span')
				->addText("Alias (Pouze malá písmena, čísla a tečka) ")
				->addHtml(
					Html::el("span", ['class' => 'aliasWrap'])
						->addText("http://{$_SERVER['HTTP_HOST']}/u/")
						->addHtml(
							Html::el('span', [
								'id' => 'aliasPreview',
								'class' => 'colorBlack fontItalic',
							])->setText('alias')
						)
				)
		);
		
		$el//->setRequired('Vyber si svŮj alias')
		->addCondition(CoreForm::FILLED, true)
			->addRule(CoreForm::PATTERN,
				"Alias může obsahovat pouze malá písmena bez diaktriky, čísla a tečku. Taktéž nesmí začínat číslem a začínat a končit tečkou.",
				self::ALIAS_PATTERN
			);
		
		$el->setHtmlId('aliasInput');
		
		return $el;
	}
	
	public function addContainer($name)
	{
		$control = new Container();
		$control->currentGroup = $this->currentGroup;
		if ($this->currentGroup !== null) {
			$this->currentGroup->add($control);
		}
		
		return $this[$name] = $control;
	}
	
	/**
	 * @param $name
	 * @param $label
	 *
	 * @internal
	 *
	 * @return Controls\ImageCropperContainer
	 */
	public function addImageCropper($name, $label)
	{
		$control = new Controls\ImageCropperContainer($this, $name, $label);
		
		$control->currentGroup = $this->currentGroup;
		
		if ($this->currentGroup !== null) {
			$this->currentGroup->add($control);
		}
		
		$control->build();
		
		return $control;
	}
	
}

/**
 * Class Container
 *
 * @package CPTeam\Forms
 */
class Container extends \Nette\Forms\Container implements Gridable
{
	use TForms;
	use TGridable;
}

trait TForms
{
	protected $id = null;
	
	/**
	 * @param $id
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		
		if (method_exists($this, "getElementPrototype")) {
			$this->getElementPrototype()->id = $this->id;
		}
		
		return $this;
	}
	
	/**
	 * @return null
	 */
	public function getId()
	{
		if ($this->id == null) {
			$this->id = "coreForm-" . uniqid();
			
			if (method_exists($this, "getElementPrototype")) {
				$this->getElementPrototype()->id = $this->id;
			}
		}
		
		return $this->id;
	}
	
	/**
	 * @param null $icon
	 * @param string $name
	 *
	 * @return FontAwesomeButton
	 */
	public function addSubmitIcon($icon = null, $name = 'submit')
	{
		return $this[$name] = new FontAwesomeButton($icon);
	}
	
	/**
	 * @param      $name
	 * @param null $default
	 * @param null $id
	 *
	 * @return Controls\HiddenField
	 */
	public function addHidden($name, $default = null, $id = null)
	{
		$input = $this[$name] = (new Controls\HiddenField)
			->setDefaultValue($default);
		
		if ($id) {
			$input->getControlPrototype()->id = $id;
		}
		
		return $input;
	}
	
	/**
	 * @param      $name
	 * @param null $default
	 * @param null $id
	 *
	 * @return Controls\HiddenField
	 */
	public function addHiddenPersistent($name, $default = null, $id = null)
	{
		$input = $this->addHidden($name, $default);
		
		if ($id) {
			$input->getControlPrototype()->id = $id;
		}
		
		$secure = $this->addHidden($name . "__secure");
		
		$this->onRender[] = function (self $form) use ($input, $secure) {
			if (!$form->hasErrors()) {
				$secure->setValue(
					$this->getPersistentSign($input)
				);
			}
		};
		
		$this->onValidate[] = function (self $form, $values) use ($input, $secure) {
			$sign = $this->getPersistentSign($input);
			
			if ($sign !== $values->{$secure->getName()}) {
				
				$form->addError("Při odeslání formuláře nastala chyba, zkus to znovu později.");
			}
		};
		
		return $input;
	}
	
	/**
	 * @param Controls\HiddenField $input
	 *
	 * @return string
	 */
	private function getPersistentSign(Controls\HiddenField $input)
	{
		$container = $this->getPresenter()->context->getService('system.container');
		
		/*
		 * Build pure unguessable hash for this
		 * - container instance
		 * - value
		 * - component
		 * - input
		 */
		
		return $hash = hash(
			"sha512",
			$input->getValue() . $input->getForm()->getName() . $input->getName() . $container->getPrivateKey()
		);
	}
	
	/**
	 * @param      $name
	 * @param null $label
	 *
	 * @return Controls\TextInput
	 */
	public function addDate($name, $label = null)
	{
		$input = $this->addText($name, $label)
			->setType('date');
		
		return $input;
	}
	
	/**
	 * @param $name
	 * @param $label
	 * @param $requiredMsg
	 *
	 * @return Controls\Checkbox
	 */
	public function addConfirm($name, $label, $requiredMsg)
	{
		$control = $this->addCheckbox($name, $label);
		
		$control->setRequired($requiredMsg);
		
		$control->setOmitted(true);
		
		$control->getLabelPart()
			->addHtml(Html::el("div", ['class' => 'colorRed'])->setText('ahahaha'));
		
		return $control;
	}
	
	/**
	 * @param      $name
	 * @param null $caption
	 *
	 * @return Controls\\Checkbox
	 */
	public function addCheckbox($name, $caption = null)
	{
		$control = $this[$name] = new Controls\Checkbox($caption);
		
		$control->getLabelPrototype()->addAttributes(['class' => 'checkboxLabel']);
		
		return $control;
	}
	
	/**
	 * @param            $name
	 * @param null $label
	 * @param array|NULL $items
	 *
	 * @return EnchancedRadioListSwitch
	 * @throws CoreFormException
	 */
	public function addSwitch($name, $label = null, array $items = null)
	{
		if (count($items) != 2) {
			throw new CoreFormException('Switch must contains two items only');
		}
		
		$control = $this[$name] = new EnchancedRadioListSwitch($label, []);
		
		foreach ($items as $key => $value) {
			$id = $name . '-' . $key;
			
			$item = Html::el('label', [
				'for' => $id,
			])
				->addHtml(
					Html::el("input", [
						'type' => 'radio',
						'name' => $name,
						'id' => $id,
						'value' => $key,
					])
				);
			
			$control->getContainer()->addHtml($item);
		}
		
		return $control;
	}
	
	/**
	 * @param       $name
	 * @param       $label
	 * @param       $handle
	 * @param array $require
	 *
	 * @return Controls\TextInput
	 * @throws \Nette\Utils\JsonException
	 */
	public function addTextWithWhisp($name, $label, $handle, array $require = [])
	{
		
		$json = [
			'control' => $this->control->getName(),
			'form' => $this->getId(),
			'url' => $this->control->link($handle),
			'require' => $require,
		];
		
		$json = Json::encode($json);
		
		$input = $this->addText($name, $label)
			->setAttribute('class', 'inputWhispInput')
			->setAttribute('autocomplete', 'off')
			->setAttribute('data-whisp', $json);
		
		return $input;
	}
	
	/* === Overide Factories ============================================================================================== */
	
	/**
	 * Adds single-line text input control to the form.
	 *
	 * @param  string  control name
	 * @param  string  label
	 * @param  int     width of the control (deprecated)
	 * @param  int     maximum number of characters the user may enter
	 *
	 * @return Controls\TextInput
	 */
	public function addText($name, $label = null, $cols = null, $maxLength = null)
	{
		return $this[$name] = (new Controls\TextInput($label, $maxLength))
			->setAttribute('size', $cols);
	}
	
	/**
	 * Adds single-line text input control used for sensitive input such as passwords.
	 *
	 * @param  string  control name
	 * @param  string  label
	 * @param  int     width of the control (deprecated)
	 * @param  int     maximum number of characters the user may enter
	 *
	 * @return Controls\TextInput
	 */
	public function addPassword($name, $label = null, $cols = null, $maxLength = null)
	{
		return $this[$name] = (new Controls\TextInput($label, $maxLength))
			->setAttribute('size', $cols)
			->setType('password');
	}
	
	/**
	 * @param      $name
	 * @param null $label
	 * @param null $cols
	 * @param null $rows
	 *
	 * @return Controls\TextArea
	 */
	public function addTextArea($name, $label = null, $cols = null, $rows = null)
	{
		return $this[$name] = (new Controls\TextArea($label))
			->setAttribute('cols', $cols)->setAttribute('rows', $rows);
	}
	
	/**
	 * Adds input for email.
	 *
	 * @param  string  control name
	 * @param  string  label
	 *
	 * @return Controls\TextInput
	 */
	public function addEmail($name, $label = null)
	{
		return $this[$name] = (new Controls\TextInput($label))
			->setRequired(false)
			->addRule(self::EMAIL);
	}
	
	/**
	 * Adds input for integer.
	 *
	 * @param  string  control name
	 * @param  string  label
	 *
	 * @return Controls\TextInput
	 */
	public function addInteger($name, $label = null)
	{
		return $this[$name] = (new Controls\TextInput($label))
			->setNullable()
			->setRequired(false)
			->addRule(self::INTEGER);
	}
	
	/**
	 * Adds control that allows the user to upload files.
	 *
	 * @param  string  control name
	 * @param  string  label
	 * @param  bool    allows to upload multiple files
	 *
	 * @return Controls\UploadControl
	 */
	public function addUpload($name, $label = null, $multiple = false)
	{
		return $this[$name] = new Controls\UploadControl($label, $multiple);
	}
	
	/**
	 * Adds control that allows the user to upload multiple files.
	 *
	 * @param  string  control name
	 * @param  string  label
	 *
	 * @return Controls\UploadControl
	 */
	public function addMultiUpload($name, $label = null)
	{
		return $this[$name] = new Controls\UploadControl($label, true);
	}
	
	/**
	 * Adds set of radio button controls to the form.
	 *
	 * @param  string  control name
	 * @param  string  label
	 * @param  array   options from which to choose
	 *
	 * @return Controls\RadioList
	 */
	public function addRadioList($name, $label = null, array $items = null)
	{
		return $this[$name] = new Controls\RadioList($label, $items);
	}
	
	/**
	 * Adds set of checkbox controls to the form.
	 *
	 * @return Controls\CheckboxList
	 */
	public function addCheckboxList($name, $label = null, array $items = null)
	{
		return $this[$name] = new Controls\CheckboxList($label, $items);
	}
	
	/**
	 * Adds select box control that allows single item selection.
	 *
	 * @param  string  control name
	 * @param  string  label
	 * @param  array   items from which to choose
	 * @param  int     number of rows that should be visible
	 *
	 * @return Controls\SelectBox
	 */
	public function addSelect($name, $label = null, array $items = null, $size = null)
	{
		return $this[$name] = (new Controls\SelectBox($label, $items))
			->setAttribute('size', $size > 1 ? (int)$size : null);
	}
	
	/**
	 * Adds select box control that allows multiple item selection.
	 *
	 * @param  string  control name
	 * @param  string  label
	 * @param  array   options from which to choose
	 * @param  int     number of rows that should be visible
	 *
	 * @return Controls\MultiSelectBox
	 */
	public function addMultiSelect($name, $label = null, array $items = null, $size = null)
	{
		return $this[$name] = (new Controls\MultiSelectBox($label, $items))
			->setAttribute('size', $size > 1 ? (int)$size : null);
	}
	
	/**
	 * Adds button used to submit form.
	 *
	 * @param  string  control name
	 * @param  string  caption
	 *
	 * @return Controls\SubmitButton
	 */
	public function addSubmit($name, $caption = null)
	{
		return $this[$name] = new Controls\SubmitButton($caption);
	}
	
	/**
	 * Adds push buttons with no default behavior.
	 *
	 * @param  string  control name
	 * @param  string  caption
	 *
	 * @return Controls\Button
	 */
	public function addButton($name, $caption = null)
	{
		return $this[$name] = new Controls\Button($caption);
	}
	
	/**
	 * Adds graphical button used to submit form.
	 *
	 * @param  string  control name
	 * @param  string  URI of the image
	 * @param  string  alternate text for the image
	 *
	 * @return Controls\ImageButton
	 */
	public function addImage($name, $src = null, $alt = null)
	{
		return $this[$name] = new Controls\ImageButton($src, $alt);
	}
	
	public function addMarkdown($name, $label = null)
	{
		$control = new Controls\MarkdownContainer();
		$control->currentGroup = $this->currentGroup;
		if ($this->currentGroup !== null) {
			$this->currentGroup->add($control);
		}
		
		$control->prepare();
		
		return $this[$name] = $control;
		
	}
	
}

class CoreFormException extends \LogicException
{
}
