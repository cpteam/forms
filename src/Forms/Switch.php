<?php
/**
 * Created by PhpStorm.
 * User: luky
 * Date: 8.6.16
 * Time: 13:47
 */

namespace CPTeam\Forms;

use Nette\Forms\Controls\RadioList;

/**
 * Class EnchancedTextArea
 */
class EnchancedRadioListSwitch extends RadioList
{
	
	public function __construct($label, array $items)
	{
		
		parent::__construct($label, $items);
		
		$this->separator = null;
		/*$this->container
			->setName("div")
			->setAttribute('class', 'radioWrap');*/
	}
	
	/**
	 * @return \Nette\Utils\Html
	 */
	public function getContainer()
	{
		return $this->container;
	}
}
